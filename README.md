# Amdal ⴰⵎⴷⴰⵍ

Amdal is a Tifinagh typeface designed by [Walid Bouchouchi - Akakir Studio](https://www.akakir.com/) released by [Velvetyne Type Foundry](http://velvetyne.fr/fonts/amdal/).

![specimen1](documentation/specimen/specimen_amdal_1.png)

Tifinagh is the alphabet used to write Tamazight, a language common to several North African countries and an official language in Algeria and Morocco. This alphabet finds its origin in antiquity, it has long fallen into disrepair, and reintroduced thanks to the committed work of linguist and historian of the region.

Today there are very few Tifinagh typefaces and Amdal is meant to be an example and an inspiration to typographers and graphic designers who use this script, in the hope of seeing more people invest in this alphabet and enrich a typographic corpus in its infancy.

Amdal is a titling font, born out of a lettering project for Korean fashion brand Merely Made, which was developing a collection inspired by the Sahara (North African desert). With the keyword "a better world". From there began the development of a family of characters reduced to writing only one sentence. Then the rest of the letters followed with Basic-IRCAM, Extended, Neo-Tifinagh and Touareg.

Thanks to Jérémy Landes and Ariel Martín Pérez for their invaluable advice and support in the realization of this font.

"Amdal" means "World" in Tamazight (Berber language).

Contribute or download it from [Velvetyne Type Foundry](http://velvetyne.fr/fonts/amdal/).

## Specimen

![specimen2](documentation/specimen/specimen_amdal_2.png)
![specimen2](documentation/specimen/specimen_amdal_3.png)
![specimen2](documentation/specimen/specimen_amdal_4.png)
![specimen2](documentation/specimen/specimen_amdal_5.png)
![specimen2](documentation/specimen/specimen_amdal_6.png)
![specimen2](documentation/specimen/specimen_amdal_7.png)

## Licence

Amdal is licensed under the SIL Open Font License, Version 1.1.
This license is copied below and is also available with an FAQ on
http://scripts.sil.org/OFL

## Layout of the repository

This font repository follows the Unified Font Repository v2.0,
a standard way to organize font project source files. Learn more about
https://github.com/unified-font-repository/Unified-Font-Repository
